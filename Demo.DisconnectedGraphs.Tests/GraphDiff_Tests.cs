using System.Linq;
using Demo.DisconnectedGraphs.Entities;
using NUnit.Framework;
using RefactorThis.GraphDiff;

namespace Demo.DisconnectedGraphs.Tests
{
    public class GraphDiff_Tests : Base_Tests
    {
        [SetUp]
        public void SetUp()
        {
            DetachedRoot = DbOps.GetRoot();
            DetachedRoot.Children.First().SubChilds.Remove(DetachedRoot.Children.First().SubChilds.First());
            DetachedRoot.Children.Add(new Child { Name = "Child #3" });
        }

        [Test]
        public void MyMethod()
        {
            using (var context = new DemoContext())
            {
                context.UpdateGraph(DetachedRoot,
                    map => map.OwnedCollection(p => p.Children,
                        with => with.OwnedCollection(c => c.SubChilds))
                    );

                context.SaveChanges();
            }

            DbOps.PrintDatabaseContents();
        }
    }
}