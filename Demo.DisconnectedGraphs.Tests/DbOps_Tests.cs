﻿using NUnit.Framework;

namespace Demo.DisconnectedGraphs.Tests
{
    public class DbOps_Tests
    {
        [Test]
        public void InsertData_Test()
        {
            DbOps.InsertData();
        } 

        [Test]
        public void RemoveAll_Test()
        {
            DbOps.RemoveAll();
        } 

        [Test]
        public void PrintDatabaseContents_Test()
        {
            DbOps.PrintDatabaseContents();
        } 
    }
}