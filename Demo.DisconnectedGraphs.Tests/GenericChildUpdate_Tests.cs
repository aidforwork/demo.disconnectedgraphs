﻿using System.Linq;
using Demo.DisconnectedGraphs.Entities;
using NUnit.Framework;

namespace Demo.DisconnectedGraphs.Tests
{
    public class GenericChildUpdate_Tests : Base_Tests
    {
        [SetUp]
        public void SetUp()
        {
            DetachedRoot = DbOps.GetRoot();

            DetachedRoot.Children.Remove(DetachedRoot.Children.First());

            DetachedRoot.Children.Add(new Child { Name = "Child #3", ParentId = DetachedRoot.Id });

            DetachedRoot.Children.First().Name += " UPDATED";
        }


        [Test]
        public void UpdateNavProperty_Test()
        {
            using (var context = new DemoContext())
            {
                var dbRoot = context.Roots.IncludeAllLevels().First();

                context.UpdateNavProperty(DetachedRoot.Children, dbRoot.Children);

                context.SaveChanges();
            }

            DbOps.PrintDatabaseContents();
        }
    }
}