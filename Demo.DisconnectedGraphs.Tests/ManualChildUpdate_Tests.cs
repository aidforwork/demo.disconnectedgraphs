using System.Collections.Generic;
using System.Linq;
using Demo.DisconnectedGraphs.Entities;
using NUnit.Framework;

namespace Demo.DisconnectedGraphs.Tests
{
    public class ManualChildUpdate_Tests : Base_Tests
    {
        [SetUp]
        public void SetUp()
        {
            DetachedRoot = DbOps.GetRoot();
            //DetachedRoot.Children.First().SubChilds.Remove(DetachedRoot.Children.First().SubChilds.First());
            DetachedRoot.Children.Remove(DetachedRoot.Children.First());
            //DetachedRoot.Children.Add(new Child() { Name = "Child #3", ParentId = DetachedRoot.Id });
        }


        [Test]
        public void UpdateNavProperty_Test()
        {
            using (var context = new DemoContext())
            {
                var dbRoot = context.Roots.IncludeAllLevels().First();

                UpdateNavProperty(DetachedRoot.Children, dbRoot.Children, context);

                context.SaveChanges();
            }

            DbOps.PrintDatabaseContents();
        }

        [Test]
        public void DeleteChild_Test()
        {
            using (var context = new DemoContext())
            {
                var dbRoot = context.Roots.IncludeAllLevels().First();

                context.Children.Remove(dbRoot.Children.First());

                context.SaveChanges();
            }

            DbOps.PrintDatabaseContents();
        }

        private void UpdateNavProperty(ICollection<Child> src, ICollection<Child> dest, DemoContext context)
        {
            var comparer = new Comparer();

            //can be optimized
            var adding = src.Except(dest, comparer).ToList();
            var removing = dest.Except(src, comparer).ToList();
            var modifying = src.Intersect(dest, comparer).ToList();

            foreach (var child in adding)
            {
                //child.Root = detachedRoot;
                dest.Add(child);
            }

            //only if we need cascade delete (mandatory ParentId property)
            foreach (var child in removing)
            {
                context.Children.Remove(child);
            }

            foreach (var child in modifying)
            {
                //map
                dest.First(c => c.Id == child.Id).Name = child.Name;
            }

        }
    }

    public class Comparer : IEqualityComparer<Child>
    {
        public bool Equals(Child x, Child y)
        {
            bool result = x.Id == y.Id && x.Id > 0;
            return result;
        }

        public int GetHashCode(Child obj)
        {
            return obj.Id;
        }
    }
}