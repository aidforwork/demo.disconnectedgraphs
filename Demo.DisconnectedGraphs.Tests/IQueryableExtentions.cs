﻿using System.Data.Entity;
using System.Linq;
using Demo.DisconnectedGraphs.Entities;

namespace Demo.DisconnectedGraphs.Tests
{
    public static class IQueryableExtentions
    {
        public static IQueryable<Root> IncludeAllLevels(this IQueryable<Root> query)
        {
            return query
                .Include(r => r.Children)
                .Include(r => r.Children.Select(c => c.SubChilds));
        }
    }
}