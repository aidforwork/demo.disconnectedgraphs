﻿using System.Linq;
using Demo.DisconnectedGraphs.Entities;
using NUnit.Framework;

namespace Demo.DisconnectedGraphs.Tests
{
    public class Base_Tests
    {

        [SetUp]
        public void SetUp()
        {
            DbOps.RemoveAll();
            DbOps.InsertData();

            DetachedRoot = DbOps.GetRoot();

            DetachedRoot.Children.First().SubChilds.Remove(DetachedRoot.Children.First().SubChilds.First());

            DetachedRoot.Children.Add(new Child { Name = "Child #3" });

        }

        public Root DetachedRoot { get; set; }
    }
}