﻿using System;
using System.Linq;
using Demo.DisconnectedGraphs.Entities;

namespace Demo.DisconnectedGraphs.Tests
{
    public static class DbOps
    {
        //[SetUp]
        public static void InsertData()
        {
            using (var context = new DemoContext())
            {
                Root root = new Root();
                root.Name = "Root #1";

                Child c1 = new Child();
                c1.Name = "Child #1";
                //c1.Root = root;

                Child c2 = new Child();
                c2.Name = "Child #2";
                //c2.Root = root;

                SubChild sc1 = new SubChild();
                sc1.Name = "SubChild #1 (Parent is Child #1)";
                //sc1.Child = c1;

                SubChild sc2 = new SubChild();
                sc2.Name = "SubChild #2 (Parent is Child #1)";
                //sc2.Child = c1;

                context.Roots.Add(root);
                root.Children.Add(c1);
                root.Children.Add(c2);
                c1.SubChilds.Add(sc1);
                c1.SubChilds.Add(sc2);

                context.SaveChanges();
            }
        }

        public static Root GetRoot()
        {
            using (var context = new DemoContext())
            {
                var roots = context.Roots
                    .IncludeAllLevels()
                    .ToList();

                if (roots.Count > 1) throw  new NotSupportedException("roots.Count > 1");

                return roots.First();
            }
        }

        public static void PrintDatabaseContents()
        {
            using (var context = new DemoContext())
            {
                var roots = context.Roots.ToList();
                var childs = context.Children.ToList();
                var subChilds = context.SubChilds.ToList();

                foreach (var root in roots)
                {
                    Console.WriteLine(root.Name);
                }
                Console.WriteLine("root count = " + roots.Count);

                foreach (var child in childs)
                {
                    Console.WriteLine(child.Name);
                }
                Console.WriteLine("child count = " + childs.Count);

                foreach (var subChild in subChilds)
                {
                    Console.WriteLine(subChild.Name);
                }
                Console.WriteLine("subchild count = " + subChilds.Count);
            }
        }

        public static void RemoveAll()
        {
            using (var context = new DemoContext())
            {
                context.Roots.RemoveRange(context.Roots);
                context.SaveChanges();
            }
        }
    }
}
