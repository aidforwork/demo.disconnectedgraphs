﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Demo.DisconnectedGraphs.Tests
{
    public static class DbContextExtentions
    {
        public static void UpdateNavProperty<T>(
            this DbContext context, 
            ICollection<T> src, 
            ICollection<T> dest)
            where T : class
        {
            //can be optimized
            var adding = src.Where(left => dest.All(right => !AreEqual(left, right))).ToList();

            var removing = dest.Where(left => src.All(right => !AreEqual(left, right))).ToList();

            var modifying = dest.Where(left => src.Any(right => AreEqual(left, right))).ToList();

            foreach (var child in adding)
            {
                dest.Add(child);
            }

            //only if we need cascade delete (mandatory ParentId property)
            foreach (var child in removing)
            {
                context.Set<T>().Remove(child);
            }

            foreach (var child in modifying)
            {
                //map
                var from = src.First(srcChild => AreEqual(srcChild, child));

                context.Entry(child).CurrentValues.SetValues(from);
            }
        }

        public static bool AreEqual(dynamic left, dynamic right)
        {
            return left.Id == right.Id;
        }
    }
}