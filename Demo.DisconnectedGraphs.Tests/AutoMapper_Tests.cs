﻿using System.Linq;
using AutoMapper;
using Demo.DisconnectedGraphs.Entities;
using NUnit.Framework;

namespace Demo.DisconnectedGraphs.Tests
{
    public class AutoMapper_Tests : Base_Tests
    {
        [SetUp]
        public void SetUp()
        {
            DetachedRoot = DbOps.GetRoot();
            DetachedRoot.Children.First().SubChilds.Remove(DetachedRoot.Children.First().SubChilds.First());
            DetachedRoot.Children.Add(new Child { Name = "Child #3" });

            var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<Root, Root>()
                );

            Mapper = config.CreateMapper();
        }

        public IMapper Mapper { get; set; }

        [Test]
        public void MyMethod()
        {
            using (var context = new DemoContext())
            {
                var dbRoot = context.Roots.IncludeAllLevels().First();

                Mapper.Map(DetachedRoot, dbRoot);

                context.SaveChanges();
            }
        }
    }
}