﻿CREATE TABLE [dbo].[Root] (
    [Id]   INT           IDENTITY (1, 1) NOT NULL,
    [Name] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Root] PRIMARY KEY CLUSTERED ([Id] ASC)
);

