﻿CREATE TABLE [dbo].[Child] (
    [Id]       INT           IDENTITY (1, 1) NOT NULL,
    [Name]     NVARCHAR (50) NOT NULL,
    [ParentId] INT           NOT NULL,
    CONSTRAINT [PK_Child] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Child_Root] FOREIGN KEY ([ParentId]) REFERENCES [dbo].[Root] ([Id]) ON DELETE CASCADE
);



