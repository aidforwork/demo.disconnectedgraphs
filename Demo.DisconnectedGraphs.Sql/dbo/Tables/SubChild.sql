﻿CREATE TABLE [dbo].[SubChild] (
    [Id]       INT           IDENTITY (1, 1) NOT NULL,
    [Name]     NVARCHAR (50) NOT NULL,
    [ParentId] INT           NOT NULL,
    CONSTRAINT [PK_SubChild] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_SubChild_Child] FOREIGN KEY ([ParentId]) REFERENCES [dbo].[Child] ([Id]) ON DELETE CASCADE
);



